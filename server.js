var express=require('express'); //inclusión de express
var app=express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000; //Puerto para express
var baseMLabURL="https://api.mlab.com/api/1/databases/apitechuibcp/collections/";
var baseWeather="http://samples.openweathermap.org/data/2.5/";
var mLabAPIKey="apiKey=_SfJ7mSPf08Ku65f0Y-5KSULb-pqwrO9";
var WeatherAPIKey="appid=b6907d289e10d714a6e88b30761fae22";
var requestJson=require('request-json');

app.listen(port); //ponemos a escuchar en el puerto.
console.log("API escuchando en el puerto " + port);


//Listado de usuarios.
app.get('/apitechu/v2/users',
function(req,res){
  console.log("GET /apitechu/v2/users");
  //Creamos cliente para la petición de usuarios.
  httpClient=requestJson.createClient(baseMLabURL);
  httpClient.get("user?"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo usuarios."
      }
      res.send(response);
    }
  )
}
);
//Listado de cuentas de un usuario concreto
app.get('/apitechu/v2/users/:id/accounts',
function(req,res){
  console.log("GET /apitechu/v2/users/:id/accounts");
  //Creamos cliente para la petición de cuentas.
  httpClient=requestJson.createClient(baseMLabURL);
  var id = req.params.id;
  var query= 'q={"userid":'+id+'}';
  httpClient.get("account?"+query+"&"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo cuentas."
      }
      res.send(response);
    }
  )
}
);
//Listado de cuentas en general
app.get('/apitechu/v2/account',
function(req,res){
  console.log("GET /apitechu/v2/account");
  httpClient=requestJson.createClient(baseMLabURL);
  //Creamos cliente para la petición de cuenta.
  httpClient.get("account?"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo cuentas."
      }
      res.send(response);
    }
  )
}
);
//Listado de movimientos de una cuenta "id"
app.get('/apitechu/v2/accounts/:id/movements',
function(req,res){
  console.log("GET /apitechu/v2/accounts/:id/movements");
  httpClient=requestJson.createClient(baseMLabURL);
  //Creamos cliente para la petición de movimientos.
  var id = req.params.id;
  var query= 'q={"idaccount":'+id+'}';
  httpClient.get("movements?"+query+"&"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo movimientos."
      }
      res.send(response);
    }
  )
}
);
//Información de usuario por id
app.get('/apitechu/v2/users/:id',
function(req,res){
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = 'q={"id":'+id+'}';
  httpClient=requestJson.createClient(baseMLabURL);
  //Creamos cliente para la petición de datos del usuario.
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err, resMLab, body) {
     if (err) {
       response = {
         "msg" : "Error obteniendo usuario."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = {
           "first_name": body[0].first_name,
           "last_name": body[0].last_name,
           "email": body[0].email,
           "country": body[0].country,
           "city": body[0].city
         };
       } else {
         response = {
           "msg" : "Usuario no encontrado."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
  )
}
);
//Login
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;
   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   //Creamos cliente para la petición de login.
   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         res.status(404);
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         var putBody = '{"$set":{"logged":true}}';
         //Creamos cliente para actualizar usuario logado en BBDD.
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             res.status(200);
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0].id,
               "city": body[0].city
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);
//Logout de usuario por su "id"
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");
   var query = 'q={"id": ' + req.params.id + '}';
   //Creamos cliente para buscar usuario antes de logout.
   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         query = 'q={"id" : ' + body[0].id +'}';
         var putBody = '{"$unset":{"logged":""}}';
         //Creamos cliente para actualizar el usuario (logged).
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);
//Alta de movimientos
app.post('/apitechu/v2/accounts/:accountid/movements',
 function(req, res) {
  console.log("POST /apitechu/v2/accounts/:accountid/movements");
  var userid = req.params.userid;
  var accountid = req.params.accountid;
  var d = new Date();
  var month = d.getMonth()+1;
  var date = d.getDate()+"/"+month+"/"+d.getFullYear();
  var time = d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
  var money = req.body.money;
  var currency = req.body.currency;
  var type = req.body.type;
  var postBody = '{"idaccount": '+accountid+',"date": "'+date+'","time": "'+time+'","money": '+money+',"currency": "'+currency+'","type": "'+type+'"}';
  var response = {
    "msg":"Error"
  }
  httpClient = requestJson.createClient(baseMLabURL);
  //Creamos cliente para actualizar BBDD con el nuevo movimiento.
  httpClient.post("movements?"+mLabAPIKey, JSON.parse(postBody),
    function(errPOST,resMLabPOST, bodyPOST) {
      if (!errPOST){
        updateAccount(accountid,money);
        response.msg="Movimientos realizado en la cuenta.";
        res.send(response);
      }
      else{
           res.send(response);
      }
    }
   );
 }
);
//Alta de nuevo usuario
app.post('/apitechu/v2/users',
 function(req, res) {
  console.log("POST /apitechu/v2/users");
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var country = req.body.country;
  var city = req.body.city;
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'"}';
  //Primero buscamos por el email para ver que no esté repetido
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err, resMLab, body) {
     if (!err){
       if (body.length > 0) {
         //Usuario ya existe. No se da de alta.
         var response = {
           "msg" : "Usuario ya registrado."
         }
         res.send(response);
       } else {
         //Usuario no está registrado.
          var query = 's={"id":-1}';
          //Cliente creado para buscar un id libre.
          httpClient = requestJson.createClient(baseMLabURL);
          httpClient.get("user?"+query+"&"+mLabAPIKey,
            function(err, resMLab, body) {
             if (!err){
               if (body.length > 0) {
                 var freeid = body[0].id + 1;
                 //Registro de nuevo usuario con la id última.
                 var postBody = '{"id": '+freeid+',"first_name": "'+first_name+'","last_name": "'+last_name+'","email": "'+email+'","country": "'+country+'","city": "'+city+'","password": "'+password+'"}';
                 httpClient.post("user?" + mLabAPIKey, JSON.parse(postBody),
                   function(errPUT, resMLabPUT, postBody) {
                     createAccount(freeid);
                     var response = {
                       "msg" : "Usuario registrado con éxito"
                     }
                     res.send(response);
                   }
                 )
               }
             }
           }
         );
       }
     }
   }
 );
}
);
//Nueva cuenta
app.post('/apitechu/v2/users/:id/accounts',
 function(req, res) {
  var userid = req.params.id;
  console.log("POST /apitechu/v2/users/:id/accounts");
  createAccount(userid);
}
);
//Funcion Nueva cuenta reutilizada en el alta y para contratación.
function createAccount(userid){
  var query = 's={"idaccount":-1}';
  //Creamos cliente http para buscar id de cuenta libre.
  httpClient = requestJson.createClient(baseMLabURL);
  var response = {
    "msg" : "Error al crear cuenta."
  }
  httpClient.get("account?"+query+"&"+mLabAPIKey,
    function(err, resMLab, body) {
     if (!err){
       if (body.length > 0) {
         var freeid = body[0].idaccount + 1;
         var freeiban;
         //Genero un iban aleatorio tomando referencia de los existentes.
         freeiban = createIbanAccount(body[0].iban);
         var balance = 0;
         //Registro de nueva cuenta con la id última.
         var postBody = '{"iban": "'+freeiban+'","idaccount": '+freeid+',"balance": '+balance+',"userid": '+userid+'}';
         httpClient.post("account?" + mLabAPIKey, JSON.parse(postBody),
           function(errPOST, resMLabPUT, postBody) {
             response.msg="Cuenta creada";
           }
         )
        }
        else{
          response.msg="Error";
          res.send(response);
        }
       }
     }
   )
}
//Crear iban nuevo a partir de referencia (preparado para hacerlo por oficina, entidad...)
function createIbanAccount(ibanref){
  var max = 9999999999;
  var min = 1000000000;
  var aux;
  aux = Math.trunc(Math.random() * (max - min) + min);
  //Concateno IBAN + Entidad + Oficina que están en ibanref y el número de cuenta y código de control generado.
  ibannew = ibanref.slice(0,-10)+aux;
  return ibannew;
}
//Actualizar balance de cuenta por movimientos
function updateAccount(idaccount,money){
  //Vemos si existe la cuenta.
  httpClient=requestJson.createClient(baseMLabURL);
  var query= 'q={"idaccount":'+idaccount+'}';
  httpClient.get("account?"+query+"&"+mLabAPIKey,
    function(err,resMLab,body){
      if(!err && body.length > 0){
        query = 'q={"idaccount" : ' + body[0].idaccount +'}';
        var balance = parseInt(body[0].balance);
        balance += parseInt(money);
        var putBody = '{"$set":{"balance":'+balance+'}}';
        //Creamos cliente para actualizar el balance de cuenta.
        httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPUT, resMLabPUT, bodyPUT) {
            console.log("Balance actualizado.");
          }
        )
      }
    }
  )
}
//Obtención del tiempo.
app.get('/apitechu/v2/weather/:city',
 function(req, res) {
  var town = req.params.city;
  console.log("/apitechu/v2/weather/:city");
  httpClient=requestJson.createClient(baseWeather);
  var query= 'q='+town;
  httpClient.get("weather?"+query+"&"+WeatherAPIKey,
    function(err,resWeather,body){
      if(err==null){
        var tempC=parseInt(body.main.temp)-273;
        console.log(tempC);
        var response = {
          "msg" : "Tiempo obtenido correctamente.",
          "temperatura" : tempC,
          "tempK": body.main.temp,
          "humedad" : body.main.humidity,
          "nubosidad" : body.clouds.all
        }
        res.send(response);
      }else {
        var response = {
          "msg" : "Error al obtener el tiempo."
        }
        res.send(response);
      }
    }
  )
}
);
//Actualizar contraseña
app.post('/apitechu/v2/users/:id/update',
 function(req, res) {
   console.log("POST /apitechu/v2/users/:id/update");
   var password = req.body.password;
   var id = req.params.id;
   var query = 'q={"id":'+id+'}';
   console.log(query);
   httpClient=requestJson.createClient(baseMLabURL);
   //Creamos cliente para la petición de datos del usuario.
   httpClient.get("user?"+query+"&"+mLabAPIKey,
     function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log(body[0]);
          query = 'q={"id" : ' + body[0].id +'}';
          console.log(query);
          var putBody = '{"$set":{"password":"'+password+'"}}';
          //Creamos cliente para actualizar usuario logado en BBDD.
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              res.status(200);
              var response = {
                "msg" : "Contraseña actualizada con éxito",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )
        } else {
          var response = {
            "msg" : "Usuario no encontrado."
          };
          res.send(response);
          res.status(404);
        }
      }
    }
   )
 }
);
