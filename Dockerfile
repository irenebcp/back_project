# Imagen raíz
FROM node

# Carperta raíz
WORKDIR /apiibcp

# Copia de archivos
ADD . /apiibcp

# Añadir volumen
#VOLUMEN ['/logs']

# Exponer Puerto
EXPOSE 3000

#Instalar dependencias
#RUN npm install

# Comando de inicialización
CMD ["npm", "start"]
